const API_URL = "http://back:8080";
const FRONT_URL = "http://front"

describe('My First Test', function() {
  it('Check server works', function() {
    cy.visit(`${FRONT_URL}`);
    cy.get('.App').should('exist');
    cy.get('.App-error').should('not.exist');
  });
});

describe('Test the API routes', function () {

  it('Check if the /add/:elem route adds the right element', function() {
    const newElementValue = "STONKS";

    cy.request(`${API_URL}/add/${newElementValue}`);
    cy.visit(`${FRONT_URL}`);
    cy.get('.my_list > li').contains(newElementValue);
  });

  it('Check if the /cleanup route deletes every items in the database', function() {
    const newElementValue = "STONKS";

    cy.request(`${API_URL}/cleanup`);
    cy.visit(`${FRONT_URL}`);
    cy.get('.my_list').should('be.empty'); // Assert that '.my_list' is empty
  });

});
