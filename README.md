# Devops I - Gwendhal CLAUDEL & Matthew LIVOTI

# [PPT de présentation rapide]

# Ce que l'on a

  - Un VPS (ssh myuser@185.229.224.220) contenant :
    -  Les containers déployés grâce à la pipeline
    -  Un gitlab runner (dind) réalisant :
        -  Un test
        -  Un build
        -  Un déploiement (en ssh)

# Selon les consignes (mandatory) :
- [x] Push and use a docker image from a registry
- [x] Create your own docker image and use docker compose
- [x] Test your application with docker
- [x] Have a working CI that test your application
- [x] You project should have at least 1 back 1 front 1 Database

[PPT de présentation rapide]: <https://epitechfr-my.sharepoint.com/:p:/g/personal/gwendhal_claudel_epitech_eu/EYhK2XVQli9Pmn0gI0N75vABT6kXOt3W4oiCO-M46-V1oQ?e=C2RFPQ>
